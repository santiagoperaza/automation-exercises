Feature: Order a menu

  As a user I should be able to order a menu.

  Background:
    Given I am on the home page

  Scenario Outline: Order a menu with two items
    Given I am on a restaurant list from "<city>" near address "<address>"
    And I select "<restaurant>" restaurant
    When I add "<menuOne>" to the order
    And I add "<menuTwo>" to the order
    And I adjust quantities of "<menuTwo>" to "<quantityMenuTwo>"
    Then I see "<menuOne>" and "<menuTwo>" under Mi Pedido
    And I see the subtotal is "<subtotal>"
    And I see the total is "<total>"

    Examples:
      | city | address | restaurant | menuOne | quantityMenuOne | menuTwo | quantityMenuTwo | subtotal | total |
      | Montevideo | 18 de julio 1249 | Facal | Sándwich caliente con jamón y queso | 1 | Ensalada de frutas | 2 | $629 | $649 |

  Scenario: Order a menu from a restaurant that accepts credit card and is the highest ranked
    Given I am on a result list of restaurants from "Montevideo" located near "Plaza Cagancha 1234" and restaurant name "La Pasiva"
    And I apply the filter "Tarjeta"
    And I select the sort option "Mejor puntuados"
    When I select the highest ranked restaurant
    And I add "Lemon pie" to the order
    And I adjust quantities of "Lemon pie" to "2"
    Then I see "2" "Lemon pie" under Mi Pedido