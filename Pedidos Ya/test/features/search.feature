Feature: Search restaurants

  As a user I should be able to search for restaurants
  based on my location and preferences.

  Background:
    Given I am on the home page

  Scenario Outline: Search restaurants by address
    When I search restaurants from "<department>" located near "<address>"
    Then I am taken to the restaurants list located near "<address>"

    Examples:
      | department | address          |
      | Maldonado  | Sarandi 792      |
      | Montevideo | 18 de Julio 1249 |

  Scenario: Search restaurants by address and apply filters
    When I search restaurants from "Montevideo" located near "Plaza Cagancha 1234"
    And I apply the filter "Tarjeta"
    And I select the category "Picadas"
    Then I verify that the filter "Tarjeta" appears in the selected filters list
    And I verify that the filter "Picadas" appears in the selected filters list
    And I print the results on the console

  Scenario: Search restaurants by address and restaurant name
    When I search restaurants from "Montevideo" located near "Plaza Cagancha 1234" and restaurant name "Gourmet"
    Then I verify that all restaurants shown have the the word "Gourmet" in their name