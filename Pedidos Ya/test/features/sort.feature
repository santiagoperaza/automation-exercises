Feature: Sort results

  As a user I should be able to sort the results list.

  Background:
    Given I am on the home page

  Scenario: Sort results by ranking 
    Given I am on a result list of restaurants from "Montevideo" located near "Plaza Cagancha 1234" and restaurant name "La Pasiva"
    When I select the sort option "Mejor puntuados"
    Then I verify that the sort option has been updated to "Mejor puntuados"