Feature: Filter results

  As a user I should be able to apply filters to the search results.

  Background:
    Given I am on the home page

  Scenario: Apply credit card filter
    Given I am on a result list of restaurants from "Montevideo" located near "Plaza Cagancha 1234" and restaurant name "La Pasiva"
    When I apply the filter "Tarjeta"
    Then I verify that the filter "Tarjeta" appears in the selected filters list