Feature: View restaurant menu

  As a user I should be able to view the menu of a particular restaurant.

  Background:
    Given I am on the home page

  Scenario Outline: View menu
    Given I am on a restaurant list from "<city>" near address "<address>"
    When I select "<restaurant>" restaurant
    Then I see the menu of the restaurant "<restaurant>"

    Examples:
      | city       | address          | restaurant |
      | Montevideo | 18 de Julio 1249 | Facal	   |