import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import searchResultsPage from '../pageobjects/searchResults.page';

defineSupportCode(function({ Given, When, Then }) {

  Given(/^I am on a result list of restaurants from "([^"]*)" located near "([^"]*)" and restaurant name "([^"]*)"$/, 
    function (city, address, restaurant) {
      homePage.buscarCompleto(city, address, restaurant)
      homePage.clickConfirmarUbicacionButton()
  })

  When(/^I apply the filter "([^"]*)"$/, function(filterName) {
    searchResultsPage.addFilter(filterName)
  })

  When(/^I select the category "([^"]*)"$/, function(categoryName) {
    searchResultsPage.selectCategory(categoryName)
  })

  Then(/^I verify that the filter "([^"]*)" appears in the selected filters list$/, function(filterName) {
    expect(searchResultsPage.validateAppliedFilter(filterName)).to.be.true
  })

})
