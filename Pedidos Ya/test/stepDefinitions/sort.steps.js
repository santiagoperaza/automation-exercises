import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import searchResultsPage from '../pageobjects/searchResults.page';

defineSupportCode(function({ Given, When, Then }) {

  When(/^I select the sort option "([^"]*)"$/, function(sortOption) {
    searchResultsPage.sortBy(sortOption)
  })

  Then(/^I verify that the sort option has been updated to "([^"]*)"$/, function(sortOption) {
    expect(searchResultsPage.getAppliedSortOption()).to.be.equal(sortOption)
  })

})
