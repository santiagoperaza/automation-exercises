import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import searchResultsPage from '../pageobjects/searchResults.page';

defineSupportCode(function({ When, Then }) {

  When(/^I search restaurants from "([^"]*)" located near "([^"]*)"$/, function(department, address) {
  	homePage.buscar(department, address)
  	homePage.clickConfirmarUbicacionButton()
  })

  Then(/^I am taken to the restaurants list located near "([^"]*)"$/, function (address) {
    expect(searchResultsPage.getResultMessage()).to.have.string(address)
  })

  When(/^I search restaurants from "([^"]*)" located near "([^"]*)" and restaurant name "([^"]*)"$/, 
    function(city, address, restaurant) {
    homePage.buscarCompleto(city, address, restaurant)
    homePage.clickConfirmarUbicacionButton()
  })

  Then(/^I verify that all restaurants shown have the the word "([^"]*)" in their name$/, function (restaurant) {
    expect(searchResultsPage.getResultsValidation(restaurant)).to.be.true
  })

  Then(/^I print the results on the console$/, function () {
    searchResultsPage.printRestaurants()
  })
})