import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import searchResultsPage from '../pageobjects/searchResults.page';
import menuPage from '../pageobjects/menu.page';
import miPedidoPage from '../pageobjects/miPedido.page';

defineSupportCode(function({ When, Then }) {

  When(/^I add "([^"]*)" to the order$/, function(menu) {
  	menuPage.selectProduct(menu)
  })

  When(/^I adjust quantities of "([^"]*)" to "([^"]*)"$/, function(menu, quantity) {
  	miPedidoPage.selectQuantity(menu, quantity)
  })

  When(/^I select the highest ranked restaurant$/, function() {
  	searchResultsPage.selectFirstRestaurant()
  })

  Then(/^I see "([^"]*)" "([^"]*)" under Mi Pedido$/, function (quantity, menu) {
    expect(miPedidoPage.validateAddedItem(menu)).to.be.true
  })
  
  Then(/^I see "([^"]*)" and "([^"]*)" under Mi Pedido$/, function (menuOne, menuTwo) {
    expect(miPedidoPage.validateAddedItem(menuOne)).to.be.true
    expect(miPedidoPage.validateAddedItem(menuTwo)).to.be.true
  })

  Then(/^I see the subtotal is "([^"]*)"$/, function (subtotal) {
    expect(miPedidoPage.getSubtotal()).to.be.equal(subtotal)
  })


  Then(/^I see the total is "([^"]*)"$/, function (total) {
    expect(miPedidoPage.getTotal()).to.be.equal(total)
  })

})