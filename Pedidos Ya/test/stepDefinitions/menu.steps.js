import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import searchResultsPage from '../pageobjects/searchResults.page';
import menuPage from '../pageobjects/menu.page';

defineSupportCode(function({ Given, When, Then }) {
  Given(/^I am on a restaurant list from "([^"]*)" near address "([^"]*)"$/, function (city, address) {
  	homePage.buscar(city, address)
  	homePage.clickConfirmarUbicacionButton()
  })

  When(/^I select "([^"]*)" restaurant$/, function(restaurant) {
  	searchResultsPage.selectRestaurant(restaurant)
  })

  Then(/^I see the menu of the restaurant "([^"]*)"$/, function (restaurant) {
    expect(menuPage.getRestaurantName()).to.be.equal(restaurant)
  })

})
