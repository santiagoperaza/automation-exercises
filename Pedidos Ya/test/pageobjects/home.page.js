import Page from './page'

class HomePage extends Page {

  get logo ()                     { return browser.element('.logo') }
  get ingresarLink ()             { return browser.element('#lnkLogin') }
  get userNameLink ()             { return browser.element('#lnkUserName > div.left.top-link') }
  get ciudadDropdown ()           { return browser.element('#selectCity_chosen a') }
  get listaDeCiudadesDropdown()   { return browser.elements('.active-result') }
  get addressField ()             { return browser.element('#address') }
  get restaurantOrFoodField ()    { return browser.element('#optional') }
  get buscarButton ()             { return browser.element('#search') }
  get confirmarUbicacionButton () { return browser.element('#confirm') }

  open () {
    //browser.reload()
    super.open('/')
    this.logo.waitForVisible()
  }

  setCityField (ciudad) {
    this.ciudadDropdown.waitForVisible()
    this.ciudadDropdown.click()

    for (let city of this.listaDeCiudadesDropdown.value) {
      if (browser.elementIdText(city.ELEMENT).value == ciudad) {
        browser.elementIdClick(city.ELEMENT)
      }
    }

  }

  setAddressField (address) {
    this.addressField.waitForVisible()
    this.addressField.setValue(address)
  }

  setRestaurantOrFoodField (restaurantOrFood) {
    this.restaurantOrFoodField.waitForVisible()
    this.restaurantOrFoodField.setValue(restaurantOrFood)
  }

  clickIngresarLink () {
    this.ingresarLink.waitForVisible()
    this.ingresarLink.click()
  }

  clickBuscarButton () {
    this.buscarButton.waitForVisible()
    this.buscarButton.click()
  }

  clickConfirmarUbicacionButton () {
    this.confirmarUbicacionButton.waitForVisible()
    browser.pause(3000)
    this.confirmarUbicacionButton.click()
  }

  getUserNameLink () {
    this.userNameLink.waitForVisible()
    return this.userNameLink.getText()
  }

  buscar (city, address) {
    this.setCityField(city)
    this.setAddressField(address)
    this.clickBuscarButton()
  }

  buscarCompleto (city, address, restaurantOrFood) {
    this.setCityField(city)
    this.setAddressField(address)
    this.setRestaurantOrFoodField(restaurantOrFood)
    this.clickBuscarButton()
  }
}

export default new HomePage()
