import Page from './page'

class MenuPage extends Page {

  get restaurant ()   { return browser.element('#profileInfo h1') }
  get productsList () { return browser.elements('.peyaCard') }

  getRestaurantName() {
    this.restaurant.waitForVisible()
    return this.restaurant.getText()
  }

  selectProduct (productParam) {
    this.productsList.waitForVisible()
      for (let product of this.productsList.value) {
        if (browser.elementIdElement(product.value.ELEMENT, ".productName").getText() == productParam) {
          browser.elementIdClick(product.ELEMENT)
          browser.pause(2000)
          break
        }
      }
  }

}

export default new MenuPage()
