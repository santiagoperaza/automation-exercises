import Page from './page'

class MiPedidoPage extends Page {

  get subtotal ()   { return browser.element('#subtotal .price') }
  get total ()      { return browser.element('.price.total-price') }
  get products ()   { return browser.elements('#cartContainer ul li') }

  getSubtotal() {
    this.subtotal.waitForVisible()
    return this.subtotal.getText()
  }

  getTotal() {
    this.total.waitForVisible()
    return this.total.getText()
  }

  selectProduct (productParam) {
    this.productsList.waitForVisible()
      for (let product of this.productsList.value) {
        if (browser.elementIdElement(product.value.ELEMENT, ".productName").getText() == productParam) {
          browser.elementIdClick(product.ELEMENT)
          break
        }
      }
  }

  selectQuantity(menu, productQuantity) {
    if (productQuantity > 1) {
      this.products.waitForVisible()
      for (let product of this.products.value) {
        if (browser.elementIdElement(product.value.ELEMENT, ".nameWrapper").getText() == menu) {
            browser.elementIdElement(product.value.ELEMENT, ".productQuantity").click()
          for (let quantity of browser.elementIdElements(product.value.ELEMENT, ".productQuantity option").value ) {
            if (browser.elementIdText(quantity.ELEMENT).value == productQuantity) {
              browser.elementIdClick(quantity.ELEMENT)
              browser.pause(2000)
              break
            }
          }
        }
      }
    }
  }

  validateAddedItem (menu) {
    this.products.waitForVisible()
    var contains = false
      for (let product of this.products.value) {
        if (browser.elementIdElement(product.value.ELEMENT, ".nameWrapper").getText()  == menu) {
          contains = true
          break
        }
      }
      return contains
  }

}

export default new MiPedidoPage()
