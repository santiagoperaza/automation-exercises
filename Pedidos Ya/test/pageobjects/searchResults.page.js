import Page from './page'

class SearchResultsPage extends Page {

  get resultCount ()                   { return browser.element('.addressTitle h1 i') }
  get results ()                       { return browser.element('.addressTitle h1') }
  get cardFilterLink ()                { return browser.element('#online-payment') }
  get sortOptionDropdown ()            { return browser.element('#resultListContainer .order a') }
  get listaDeRestaurantes ()           { return browser.elements('.restaurant-wrapper h3 a') }
  get listaDeRestaurantesCompleta ()   { return browser.elements('.restaurant-wrapper.peyaCard') }
  get filtersList ()                   { return browser.elements('.js-filter-channel.old_channel a span') }
  get categoriesList ()                { return browser.elements('#foodCategories span') }
  get sortOptionsList ()               { return browser.elements('.order li a') }
  get appliedFiltersList ()            { return browser.elements('.appliedFilter span') }
  get firstRestaurantResult ()         { return browser.element('#resultList .restaurantData') }
  get selectedSortOption ()            { return browser.element('.order > a') }

  getResultsCount () {
    this.resultCount.waitForVisible()
    return this.resultCount.getText()
  }

  getResultMessage () {
    this.results.waitForVisible()
    return this.results.getText()
  }

  selectRestaurant (restaurantParam) {
    this.listaDeRestaurantes.waitForVisible()
    for (let restaurant of this.listaDeRestaurantes.value) {
      if (browser.elementIdText(restaurant.ELEMENT).value == restaurantParam) {
        browser.elementIdClick(restaurant.ELEMENT)
        break
      }
    }
  }

  selectFirstRestaurant () {
    this.firstRestaurantResult.waitForVisible()
    this.firstRestaurantResult.click()
  }

  getResultsValidation (restaurantParam) {
    this.listaDeRestaurantes.waitForVisible()
    var contains = true
    for (let restaurant of this.listaDeRestaurantes.value) {
      if (!browser.elementIdText(restaurant.ELEMENT).value.includes(restaurantParam)) {
        contains = false
      }
    }
    return contains
  }

  addFilter(filterName) {
    this.filtersList.waitForVisible()
    for (let filter of this.filtersList.value) {
      if (browser.elementIdText(filter.ELEMENT).value == filterName) {
        browser.elementIdClick(filter.ELEMENT)
        break
      }
    }
  }

  selectCategory(categoryName) {
    this.filtersList.waitForVisible()
    for (let category of this.categoriesList.value) {
      if (browser.elementIdText(category.ELEMENT).value == categoryName) {
        browser.elementIdClick(category.ELEMENT)
        browser.pause(3000)
        break
      }
    }
  }

  validateAppliedFilter (filterName) {
    this.appliedFiltersList.waitForVisible()
    var contains = false
    for (let appliedFilter of this.appliedFiltersList.value) {
      if (browser.elementIdText(appliedFilter.ELEMENT).value == filterName) {
        contains = true
        break
      }
    }
    return contains
  }

  printRestaurants () {
    this.listaDeRestaurantesCompleta.waitForVisible()
    for (let restaurant of this.listaDeRestaurantesCompleta.value) {
      console.log("Restaurant name: " + browser.elementIdElement(restaurant.value.ELEMENT, 'h3 a').getText())
      if (browser.elementIdElement(restaurant.value.ELEMENT, '.open-time').isExisting() ||
        browser.elementIdElement(restaurant.value.ELEMENT, '.open-time').isExisting()) {
        console.log("Is it opened: No")
      } else {
        console.log("Is it opened: Yes")
      }
      console.log("Restaurant rating: " + browser.elementIdElement(restaurant.value.ELEMENT, '.rating-points').getText())
      console.log("Restaurant address: " + browser.elementIdElement(restaurant.value.ELEMENT, '.address').getText())
      console.log("Time of delivery: " + browser.elementIdElement(restaurant.value.ELEMENT, '.delTime').getText())
      if (browser.elementIdElement(restaurant.value.ELEMENT, '.shipping i').getText() == 'Gratis') {
        console.log("Is delivery free: Yes")
      } else {
        console.log("Is delivery free: No - " + browser.elementIdElement(restaurant.value.ELEMENT, '.shipping i').getText())
      }
      console.log("\n")
    }
  }

  sortBy (sortOptionParam) {
    this.sortOptionDropdown.waitForVisible()
    this.sortOptionDropdown.click()
    this.sortOptionsList.waitForVisible()
    for (let sortOption of this.sortOptionsList.value) {
      if (browser.elementIdText(sortOption.ELEMENT).value == sortOptionParam) {
        browser.elementIdClick(sortOption.ELEMENT)
        break
      }
    }
  }

  getAppliedSortOption () {
    this.selectedSortOption.waitForVisible()
    return this.selectedSortOption.getText()
  }

}

export default new SearchResultsPage()
