import { defineSupportCode } from 'cucumber'
import formAuthenticationPage from '../pageObjects/formAuthentication.page'

defineSupportCode(function({ When, Then }) {

	When(/^I log in with credentials "([^"]*)" and "([^"]*)"$/, function(username, password) {
		formAuthenticationPage.setCredentials(username, password)
		formAuthenticationPage.clickLoginButton()
	})

	Then(/^I see the message "([^"]*)"$/, function(errorMessage) {
		expect(formAuthenticationPage.getErrorMessage()).to.have.string(errorMessage)
	})
})