import { defineSupportCode } from 'cucumber'
import dragAndDropPage from '../pageObjects/dragAndDrop.page'

defineSupportCode(function({ When }) {

	When(/^I drag box "([^"]*)" into box "([^"]*)"$/, function(a, b) {
		dragAndDropPage.dragAndDropAintoB()
	})

})