import { defineSupportCode } from 'cucumber'
import homePage from '../pageObjects/home.page'
import dropdownPage from '../pageObjects/dropdown.page'

defineSupportCode(function({ When, Then }) {

	When(/^I select the dropdown option "([^"]*)"$/, function(option) {
		dropdownPage.selectDropdownOption(option)
	})

	Then(/^I validate that the selected option is "([^"]*)"$/, function(option) {
		expect(dropdownPage.getSelectedOption()).to.be.equal(option)
	})

})