import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';

defineSupportCode(function({ Given, When }) {

  Given(/^I am on the home page$/, function() {
    homePage.open()
  })

  When(/^I select the option "([^"]*)"$/, function(content) {
	homePage.clickOption(content)
  })

})
