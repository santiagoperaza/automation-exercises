import { defineSupportCode } from 'cucumber'
import homePage from '../pageObjects/home.page'
import dynamicContentPage from '../pageObjects/dynamicContent.page'

defineSupportCode(function({ When, Then }) {

	When(/^I print on the console the avatar information$/, function() {
		dynamicContentPage.printInformation()
		dynamicContentPage.saveScreenshot()
		dynamicContentPage.refreshThePage()
		dynamicContentPage.printInformation()
		dynamicContentPage.saveScreenshot()
	})

})