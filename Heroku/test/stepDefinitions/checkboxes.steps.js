import { defineSupportCode } from 'cucumber'
import homePage from '../pageObjects/home.page'
import checkboxesPage from '../pageObjects/checkboxes.page'

defineSupportCode(function({ When, Then }) {

	When(/^I select checkbox "([^"]*)"$/, function(option) {
		checkboxesPage.selectCheckbox(option)
	})

	When(/^I deselect checkbox "([^"]*)"$/, function(option) {
		checkboxesPage.deselectCheckbox(option)
	})

	Then(/^I verify checkbox "([^"]*)" is selected$/, function(option) {
		expect(checkboxesPage.isChecked(option)).to.be.true
	})

	Then(/^I verify checkbox "([^"]*)" is not selected$/, function(option) {
		expect(checkboxesPage.isChecked(option)).to.be.false
	})

})