import { defineSupportCode } from 'cucumber'
import homePage from '../pageObjects/home.page'
import downloadFilePage from '../pageObjects/downloadFile.page'

defineSupportCode(function({ When, Then }) {

	When(/^I download the file "([^"]*)"$/, function(fileName) {
		downloadFilePage.downloadFile(fileName)
	})

	Then(/^I print on the console the file content of file "([^"]*)"$/, function(fileName) {
		downloadFilePage.printFile(fileName)
	})

})