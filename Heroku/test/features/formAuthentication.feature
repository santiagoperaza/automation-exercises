Feature: Form Authentication

  As a user, I should be able to perform different actions on Form Authentication example.

  Background:
    Given I am on the home page
    And I select the option "Form Authentication"

  Scenario: Attempt to log in with invalid credentials
    When I log in with credentials "user" and "12345678"
    Then I see the message "Your username is invalid!"

  Scenario: Log in with valid credentials
    When I log in with credentials "tomsmith" and "SuperSecretPassword!"
    Then I see the message "You logged into a secure area!"