Feature: Checkboxes

  As a user, I should be able to perform different actions on Checkboxes example.

  Background:
    Given I am on the home page
    And I select the option "Checkboxes"

  Scenario: Select a checkbox
    When I select checkbox "1"
    Then I verify checkbox "1" is selected

  Scenario: Deselect a checkbox
    When I deselect checkbox "2"
    Then I verify checkbox "2" is not selected