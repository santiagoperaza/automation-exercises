Feature: File Download

  As a user, I should be able to perform different actions on File Download example.

  Background:
    Given I am on the home page
    And I select the option "File Download"

  Scenario Outline: Download a txt file
    When I download the file "<file>"
    Then I print on the console the file content of file "<file>"

    Examples:
      | file |
      | foo.txt |