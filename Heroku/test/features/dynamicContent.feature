Feature: Dynamic Content

  As a user, I should be able to perform different actions on Dynamic Content example.

  Background:
    Given I am on the home page
    And I select the option "Dynamic Content"

  Scenario: Show on the console the link to the avatar and text of each post
    When I print on the console the avatar information