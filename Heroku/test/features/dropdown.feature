Feature: Dropdown

  As a user, I should be able to perform different actions on Dropdown example.

  Background:
    Given I am on the home page
    And I select the option "Dropdown"

  Scenario Outline: Click on Dropdown link
    When I select the dropdown option "<option>"
    Then I validate that the selected option is "<option>"

    Examples:
      | option |
      | Option 1 |
      | Option 2 |