Feature: Drag and drop

  As a user, I should be able to perform different actions on Drag and Drop example.

  Background:
    Given I am on the home page
    And I select the option "Drag and Drop"

  Scenario Outline: Drag and drop boxes
    When I drag box "<originBox>" into box "<destinationBox>"

    Examples:
      | originBox | destinationBox |
      | A | B |
      | B | A |