import Page from './page'

class DynamicContentPage extends Page {

	get postsList ()		{ return browser.elements('#content > .row') }

	printInformation () {
		this.postsList.waitForVisible()
		for (let post of this.postsList.value) {
			console.log("Avatar: " + browser.elementIdElement(post.value.ELEMENT, 'img').getAttribute('', 'src'))
			console.log("Text: " + browser.elementIdElement(post.value.ELEMENT, '.large-10.columns').getText() + '\n')
		}
	}

	refreshThePage () {
		browser.refresh()
	}

	saveScreenshot () {
		// TODO Should obtain relative path
		browser.saveScreenshot('/Users/santiagoperaza/Desktop/Automation Exercices/Heroku/test/reports/screenshots/' + Date.now() + '.png')
	}

}

export default new DynamicContentPage()