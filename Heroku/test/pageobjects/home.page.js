import Page from './page'

class HomePage extends Page {

  get homePageTitle ()            { return browser.element('h2') }
  get contentList ()              { return browser.elements('li a') }

  open () {
    super.open('/')
    this.homePageTitle.waitForVisible()
  }

  getPageTitle () {
    this.homePageTitle.waitForVisible()
    return this.homePageTitle.getText()
  }

  clickOption (option) {

    this.contentList.waitForExist()
    for (let content of this.contentList.value) {
      if (browser.elementIdText(content.ELEMENT).value == option) {
        browser.elementIdClick(content.ELEMENT)
        break
      }
    }

  }

}

export default new HomePage()
