import Page from './page'

class CheckboxesPage extends Page {

	get pageTitle ()		{ return browser.element('.example h3') }
	get checkboxes ()		{ return browser.elements('#checkboxes') }

	getPageTitle () {
	  this.pageTitle.waitForVisible()
	  return this.pageTitle.getText()
	}

	selectCheckbox (optionParam) {
	  this.checkboxes.waitForVisible()
	  browser.execute( 	function (optionParam) {
	  	var selector = '#checkboxes input:nth-of-type(' + optionParam + ')'
	    document.querySelector(selector).checked = true
	  }, optionParam )
	}

	deselectCheckbox (optionParam) {
	  this.checkboxes.waitForVisible()
	  browser.execute( function (optionParam) {
	  	var selector = '#checkboxes input:nth-of-type(' + optionParam + ')'
	    document.querySelector(selector).checked = false
	  }, optionParam )
	}


	isChecked (optionParam) {
	  this.checkboxes.waitForVisible()
	  console.log("IS SELECTED: " + browser.isSelected('#checkboxes input:nth-of-type(' + optionParam + ')'))
	  return browser.isSelected('#checkboxes input:nth-of-type(' + optionParam + ')')
	}

}

export default new CheckboxesPage()