import Page from './page'

class FormAuthenticationPage extends Page {

	get usernameField () 		{ return browser.element('#username') }
	get passwordField () 		{ return browser.element('#password') }
	get loginButton ()		 	{ return browser.element('.fa-sign-in') }
	get errorMessage ()			{ return browser.element('#flash') }

	setUsernameField (username) {
	  this.usernameField.waitForVisible()
	  this.usernameField.setValue(username)
	}

	setPasswordField (password) {
	  this.passwordField.waitForVisible()
	  this.passwordField.setValue(password)
	}

	clickLoginButton () {
	  this.loginButton.waitForVisible()
	  this.loginButton.click()
	}

	setCredentials (username, password) {
	  this.setUsernameField(username)
	  this.setPasswordField(password)
	}

	getErrorMessage () {
	  this.errorMessage.waitForVisible()
	  return this.errorMessage.getText()
	}
	
}

export default new FormAuthenticationPage()