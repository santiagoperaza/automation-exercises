import Page from './page'

class DropdownPage extends Page {

	get pageTitle ()			{ return browser.element('.example h3') }
	get dropdownOption ()		{ return browser.element('#dropdown') }
	get dropdownOptions ()		{ return browser.elements('#dropdown option') }

	selectDropdownOption (optionParam) {
	  this.dropdownOptions.waitForVisible()
	  this.dropdownOption.click()

      for (let option of this.dropdownOptions.value) {
	    if (browser.elementIdText(option.ELEMENT).value == optionParam) {
	      browser.elementIdClick(option.ELEMENT)
		  break
	    }
	  }
	}

	getPageTitle () {
	  this.pageTitle.waitForVisible()
	  return this.pageTitle.getText()
	}

	getSelectedOption () {
	  var returnOption = browser.execute( () => {
	    var selectedOption = document.getElementById('dropdown')
	    return selectedOption.options[selectedOption.selectedIndex].text
	  })
	  return returnOption.value
	}
}

export default new DropdownPage()