import Page from './page'

class DragAndDropPage extends Page {

	get pageTitle ()		{ return browser.element('.example h3') }
	get checkboxA ()		{ return browser.element('#column-a') }
	get checkboxB ()		{ return browser.element('#column-b') }

	getPageTitle () {
	  this.pageTitle.waitForVisible()
	  return this.pageTitle.getText()
	}

	dragAndDropAintoB () {
	  this.checkboxA.waitForVisible()
	  this.checkboxB.waitForVisible()

	  browser.dragAndDrop(this.checkboxA.selector, this.checkboxB.selector)
	  browser.pause(2000)
	}

}

export default new DragAndDropPage()