import Page from './page'

class DownloadFilePage extends Page {

	get pageTitle ()		{ return browser.element('.example h3') }
	get files ()			{ return browser.elements('.example a') }

	downloadFile (fileName) {
	  this.files.waitForVisible()

	  for (let file of this.files.value) {
	  	if (browser.elementIdText(file.ELEMENT).value.includes(fileName)) {
			browser.elementIdClick(file.ELEMENT)
			break
	  	}
	  }
	}

	printFile(file) {
	  // TODO Should obtain relative path
	  const filePath = 'file:///Users/santiagoperaza/Downloads/' + file
	  var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
      var rawFile = new XMLHttpRequest();
      rawFile.open("GET", filePath, false);
      rawFile.onreadystatechange = function (){
        if(rawFile.readyState === 4) {
          if(rawFile.status === 200 || rawFile.status == 0) {
            var allText = rawFile.responseText;
            console.log(allText);
          }
        }
      }
      rawFile.send(null);
    }

}

export default new DownloadFilePage()