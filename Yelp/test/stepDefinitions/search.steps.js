import { defineSupportCode } from 'cucumber'
import homePage from '../pageObjects/home.page';
import searchResultsPage from '../pageObjects/searchResults.page';
import productPage from '../pageObjects/product.page';

defineSupportCode(function({ Given, When, Then }) {

  Given(/^I am on the search results page of "([^"]*)" with title "([^"]*)"$/, function(option, pageTitle) {
  	homePage.search(option)
  	expect(searchResultsPage.getPageTitle()).to.have.string(pageTitle)
  })

  When(/^I select "([^"]*)" from the dropdown$/, function(option) {
  	homePage.search(option)
  })

  When(/^I append "([^"]*)" to the search bar$/, function(textToAppend) {
  	searchResultsPage.appendTextToFind(textToAppend)
  })

  When(/^I click the search button$/, function() {
  	searchResultsPage.clickSearchButton()
  })

  When(/^I select the first result$/, function() {
    searchResultsPage.selectFirstResult()
  })

  Then(/^I am taken to the next page with title "([^"]*)"$/, function (pageTitle) {
    expect(searchResultsPage.getPageTitle()).to.have.string(pageTitle)
  })

  Then(/^I validate that I am taken to the reults page by checking "([^"]*)" button is present$/, function (buttonText) {
    expect(productPage.getWriteReviewButton()).to.have.string(buttonText)
    productPage.logDetails()
    productPage.logReviews()
  })

})