import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import searchResultsPage from '../pageobjects/searchResults.page';
import filterPage from '../pageobjects/filter.page';

defineSupportCode(function({ Given, When, Then }) {

  Given(/^I am on a result list of restaurants from "([^"]*)" located near "([^"]*)" and restaurant name "([^"]*)"$/, 
    function (city, address, restaurant) {
      homePage.buscarCompleto(city, address, restaurant)
      homePage.clickConfirmarUbicacionButton()
  })

  When(/^I click the button All Filters and click More Neighborhoods$/, function() {
    filterPage.clickAllFilters()
    filterPage.clickMoreNeighborhoods()
  })

  When(/^I select the neighborhood "([^"]*)"$/, function(neighborhood) {
    filterPage.selectNeighborhood(neighborhood)
  })

  When(/^I click the search button on More Neighborhoods modal$/, function() {
    filterPage.clickSearchButtonOk()
  })

  When(/^I select the distance "([^"]*)"$/, function(distanceOption) {
  	filterPage.clickAllFilters()
    filterPage.selectFilter(distanceOption)
  })

  Then(/^I see the header contains "([^"]*)"$/, function (pageTitle) {
    expect(searchResultsPage.getResultsLocation()).to.have.string(pageTitle)
  })

  Then(/^I see that the selected Distance value is "([^"]*)"$/, function (distanceOption) {
    expect(filterPage.isFilterSelected(distanceOption)).to.be.true
  })

})
