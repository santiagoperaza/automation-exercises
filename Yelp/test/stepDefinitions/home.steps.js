import { defineSupportCode } from 'cucumber'
import homePage from '../pageObjects/home.page';

defineSupportCode(function({ Given, When }) {
  Given(/^I am on the home page$/, function() {
    homePage.open()
  })

  When(/^I search "([^"]*)" near "([^"]*)"$/, function(city, term) {
    homePage.searchAPI(city, term)
  })
})