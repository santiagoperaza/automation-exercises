import Page from './page'

class FilterPage extends Page {

get allOptions ()                     { return browser.elements('.filter__373c0__2M8m3') }
get allElements ()                    { return browser.elements('.lemon--p__373c0__3Qnnj') }
get listOfNeighborhoods ()            { return browser.elements('li[class^=lemon--li] input[type=checkbox] + span') }
get listOfButtons ()                  { return browser.elements('button[class^=button__373c0] > span') }
get filterElements ()                 { return browser.elements('li[class^=lemon--li__373c0__1r9wz]') }
get filterElementsSelected ()         { return browser.elements('li[class^=lemon--li__373c0__1r9wz] label[class*=selected__373c0__3jVmO]') }
get loadingSpinner ()                 { return browser.element('div[style^="transform"]') }

clickAllFilters () {
  this.allOptions.waitForVisible()

  for (let option of this.allOptions.value) {
    if (browser.elementIdText(option.ELEMENT).value == " All Filters") {
      browser.elementIdClick(option.ELEMENT)
    }
  }
}

clickMoreNeighborhoods () {
  this.allElements.waitForExist()
  for (let option of this.allElements.value) {
    if (browser.elementIdText(option.ELEMENT).value == "More Neighborhoods") {
      browser.elementIdClick(option.ELEMENT)
    }
  }  
}

selectNeighborhood (neighborhood) {
  this.listOfNeighborhoods.waitForExist()
  for (let option of this.listOfNeighborhoods.value) {
    if (browser.elementIdText(option.ELEMENT).value == neighborhood) {
      browser.elementIdClick(option.ELEMENT)
    }
  }
}

clickSearchButtonOk () {
  this.listOfButtons.waitForVisible()

  for (let button of this.listOfButtons.value) {
    if (browser.elementIdText(button.ELEMENT).value == "Search") {
      browser.elementIdClick(button.ELEMENT)
    }
  }
}

selectFilter (filterOption) {
  this.filterElements.waitForVisible()
  for (let filter of this.filterElements.value) {
    if (browser.elementIdText(filter.ELEMENT).value == filterOption) {
      browser.elementIdClick(filter.ELEMENT)
    }
  }
}

isFilterSelected (filterOption) {
  while (this.loadingSpinner.isExisting()) {
    browser.pause(500)
  }
  this.filterElementsSelected.waitForVisible()
  var isSelected = false
  for (let selectedFilter of this.filterElementsSelected.value) {
    if (browser.elementIdText(selectedFilter.ELEMENT).value == filterOption) {
      isSelected = true
    }
  }
  return isSelected
}

}

export default new FilterPage()