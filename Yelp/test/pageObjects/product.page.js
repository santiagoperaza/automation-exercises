import Page from './page'

class ProductPage extends Page {

  get writeReviewButton ()      { return browser.element('.ybtn.ybtn--primary.war-button') }
  get product ()				{ return browser.element('.biz-page-title') }
  get address ()				{ return browser.element('.mapbox .street-address') }
  get phone ()					{ return browser.element('.mapbox .biz-phone') }
  get website ()				{ return browser.element('.mapbox .biz-website') }
  get reviews ()				{ return browser.elements('.review-content p') }

  getWriteReviewButton() {
  	this.writeReviewButton.waitForVisible()
  	return this.writeReviewButton.getText()
  }

  logDetails () {
  	this.address.waitForVisible()
  	this.phone.waitForVisible()
  	this.website.waitForVisible()
  	this.product.waitForVisible()
  	console.log("\n**************************************************************")
  	console.log("\nRestaurant: " + this.product.getText())
  	console.log("Address: " + this.address.getText())
  	console.log("Phone: " + this.phone.getText())
  	console.log("WebSite: " + this.website.getText())
  }

  logReviews () {
  	this.reviews.waitForVisible()
  	var indice = 1
  	for (let review of this.reviews.value) {
  		if (indice < 4) {
  			console.log("\n**************************************************************")
  			console.log("\nReview " + indice + ": " + browser.elementIdText(review.ELEMENT).value)
  		}
  		indice++
  	}
  }

}

export default new ProductPage()