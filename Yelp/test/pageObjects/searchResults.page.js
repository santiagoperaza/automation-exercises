import Page from './page'

class SearchResultsPage extends Page {

  get logo ()                 { return browser.element('#logo') }
  get findDropdown ()         { return browser.element('#find_desc') }
  get findOptions ()		      { return browser.elements('.item') }
  get searchInput ()          { return browser.element('#search_description') }
  get searchButton ()         { return browser.element('button[data-testid="suggest-submit"]') }
  get loadingSpinner ()       { return browser.element('div[style^="transform"]') }
  get numberOfResults ()      { return browser.element('h1.lemon--h1__373c0__2ZHSL + p') }
  get resultsLocation ()      { return browser.element('.queryLocation__373c0__15viw') }
  get listOfResults ()        { return browser.element('ul[class^="lemon--ul__373c0__1_cxs undefined list__373c0__2G8oH"] li h3 a') }

  open () {
  	super.open('/')
  	this.logo.waitForVisible()
  }

  clickFindDropdown() {
  	this.findDropdown.waitForVisible()
  	this.findDropdown.click()
  }

  search (findOption) {
  	this.clickFindDropdown()
  	this.findOptions.waitForVisible()

  	for (let option of this.findOptions.value) {
  	  if (browser.elementIdText(option.ELEMENT).value == findOption) {
  	  	browser.elementIdClick(option.ELEMENT)
  	  }
  	}
 	
  }

  getPageTitle () {
    while (this.loadingSpinner.isExisting()) {
      browser.pause(500)
    }
    this.logNumberOfResults()
    return browser.getTitle()
  }

  getResultsLocation () {
    while (this.loadingSpinner.isExisting()) {
      browser.pause(500)
    }
    this.resultsLocation.waitForVisible()
    return this.resultsLocation.getText()
  }

  appendTextToFind (textToAppend) {
    this.searchInput.waitForVisible()
    this.searchInput.setValue(" " + textToAppend)
  }

  clickSearchButton () {
    this.searchButton.waitForVisible()
    this.searchButton.click()
  }

  logNumberOfResults () {
    const result = browser.execute((str) => {
        var myRegexp = /of (\d+)/g
        var regexResult = myRegexp.exec(str)
        return regexResult[1]
    }, this.numberOfResults.getText())
    console.log("Number of search results for '" + this.searchInput.getValue() + "': " + result.value)
  }

  selectFirstResult () {
    this.listOfResults.waitForExist()
    this.listOfResults.click()
  }

}

export default new SearchResultsPage()