import Page from './page'

class HomePage extends Page {

  get logo ()				{ return browser.element('#logo') }
  get findDropdown()		{ return browser.element('#find_desc') }
  get findOptions()			{ return browser.elements('.item') }

  open () {
  	super.open('/')
  	this.logo.waitForVisible()
  }

  clickFindDropdown() {
  	this.findDropdown.waitForVisible()
  	this.findDropdown.click()
  }

  search (findOption) {
  	this.clickFindDropdown()
  	this.findOptions.waitForVisible()

  	for (let option of this.findOptions.value) {
  	  if (browser.elementIdText(option.ELEMENT).value == findOption) {
  	  	browser.elementIdClick(option.ELEMENT)
  	  }
  	}
  	
  }

}

export default new HomePage()