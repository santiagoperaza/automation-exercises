Feature: Filter

As a user I should be able to filter the search results

Background:
  Given I am on the home page
  And I am on the search results page of "Restaurants" with title "Top 10 Best Restaurants"

Scenario Outline: Filter Results by Neighborhoods
  When I click the button All Filters and click More Neighborhoods
  And I select the neighborhood "<neighborhood>"
  And I click the search button on More Neighborhoods modal
  Then I see the header contains "near <neighborhood>"

  Examples:
    | neighborhood |
    | Fillmore |
    | Castro |

Scenario Outline: Filter Results by Distance
  When I select the distance "<distance>"
  Then I see that the selected Distance value is "<distance>"

  Examples:
    | distance 			|
	| Bird's-eye View 	|
	| Driving (5 mi.)	|
	| Walking (1 mi.)	|
	| Within 4 blocks	|