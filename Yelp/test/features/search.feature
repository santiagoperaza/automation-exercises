Feature: Search

As a user I should be able to search for the available options.

Background:
  Given I am on the home page

Scenario: Search Restaurants
  When I select "Restaurants" from the dropdown
  Then I am taken to the next page with title "Top 10 Best Restaurants"

Scenario: Append Pizza to the search text
  Given I am on the search results page of "Restaurants" with title "Top 10 Best Restaurants"
  When I append "Pizza" to the search bar
  And I click the search button
  Then I am taken to the next page with title "Top 10 Best Restaurants Pizza"

Scenario: Select first result
  Given I am on the search results page of "Restaurants" with title "Top 10 Best Restaurants"
  When I select the first result
  Then I validate that I am taken to the reults page by checking "Write a Review" button is present