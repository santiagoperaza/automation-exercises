import { defineSupportCode } from 'cucumber'
import signUpPage from '../pageobjects/signup.page';

defineSupportCode(function({ Given, When, Then }) {

  Given(/^The email "([^"]*)" is already registered$/, function(email) {
    signUpPage.setEmailField(email)
    signUpPage.clickNextButton()
    expect(signUpPage.getEmailValidationText()).to.be.equal("This email address is taken.")
  })

  Given(/^I am on the page to enter the wedding date$/, function() {
    signUpPage.clickLetsDoThisButton()
    signUpPage.setFirstNameField('Test One')
    signUpPage.setLastNameField('Testing')
    signUpPage.clickNextButton()
    signUpPage.clickLetsDoThisButton()
    signUpPage.setSpouseFirstNameField('Test Two')
    signUpPage.setSpouseLastNameField('Testing')
    signUpPage.clickNextButton()
  })

  When(/^I enter valid email and password$/, function(table) {
    const hashes = table.hashes()
    let email = hashes[0]['email']
    let pwd = hashes[0]['password']

    signUpPage.setEmailField(email)
    signUpPage.setPasswordField(pwd)
    signUpPage.clickNextButton()
  })

  When(/^I enter email "([^"]*)"$/, function(email) {
    signUpPage.setEmailField(email)
    signUpPage.clickNextButton()
  })

  When(/^I enter password "([^"]*)"$/, function(password) {
    signUpPage.setPasswordField(password)
    signUpPage.clickNextButton()
  })

  When(/^I click sign up$/, function() {
    signUpPage.clickNextButton()
  })

  When(/^I select the date "([^"]*)"\/"([^"]*)"\/"([^"]*)"$/, function(day, month, year) {
    signUpPage.selectWeddingDate(day, month, year)
  })

  When(/^I complete the required fields with valid data$/, function() {
    signUpPage.setFirstNameField('Test One')
    signUpPage.setLastNameField('Testing')
    signUpPage.clickNextButton()
    signUpPage.clickLetsDoThisButton()
    signUpPage.setSpouseFirstNameField('Test Two')
    signUpPage.setSpouseLastNameField('Testing')
    signUpPage.clickNextButton()
    signUpPage.selectWeddingMonth('June')
    signUpPage.selectWeddingDateDay('15')
    signUpPage.selectWeddingYear('2020')
    signUpPage.clickNextButton()
    signUpPage.setGuestsInvited()
    signUpPage.clickNextButton()
    signUpPage.selectEmoticon()
    signUpPage.selectOtherStoresLabel()
    signUpPage.clickNextButton()
    signUpPage.selectFreeShippingLabel()
    signUpPage.selectGiftOption()
    signUpPage.selectThingsTogether()
    signUpPage.clickNextButton()
    signUpPage.selectSpendLabel()
    signUpPage.setEmailField('testmai1007@testing.com')
    signUpPage.setPasswordField('12345678')
    signUpPage.clickNextButton()
    signUpPage.clickSkipGiftButton()
  })

  Then(/^I am taken to the next page with text "([^"]*)"$/, function(assertText) {
    expect(signUpPage.getNextPageValidationText()).to.have.string(assertText)
  })

  Then(/^I see "([^"]*)" message under email field$/, function(assertText) {
    expect(signUpPage.getEmailValidationText()).to.be.equal(assertText)
  })

  Then(/^I see "([^"]*)" message under password field$/, function(assertText) {
    expect(signUpPage.getPasswordValidationText()).to.be.equal(assertText)
  })

  Then(/^I see "([^"]*)" message under wedding date field$/, function(assertText) {
    expect(signUpPage.getWeddingDateValidationText()).to.be.equal(assertText)
  })

  Then(/^I do not see "([^"]*)" message under password field$/, function(assertText) {
    expect(signUpPage.getPasswordValidationText()).to.not.be.equal(assertText)
  })

  Then(/^I do not see "([^"]*)" message under email field$/, function(assertText) {
    expect(signUpPage.getEmailValidationText()).to.not.be.equal(assertText)
  })

  Then(/^I do not see "([^"]*)" message under wedding date field$/, function(assertText) {
    expect(signUpPage.getWeddingDateValidationText()).to.not.be.equal(assertText)
  })

})
