import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import signUpPage from '../pageobjects/signup.page';
import loginPage from '../pageobjects/login.page';

defineSupportCode(function({ Given, When }) {
  Given(/^I am on the page to enter email and password$/, function() {
    homePage.open()
    homePage.clickSignUpLink()
    homePage.clickRegistry()
    signUpPage.clickLetsDoThisButton()
    signUpPage.setFirstNameField('Test One')
    signUpPage.setLastNameField('Testing')
    signUpPage.clickNextButton()
	signUpPage.clickLetsDoThisButton()
	signUpPage.setSpouseFirstNameField('Test Two')
    signUpPage.setSpouseLastNameField('Testing')
    signUpPage.clickNextButton()
    signUpPage.selectWeddingMonth('June')
    signUpPage.selectWeddingDateDay('15')
    signUpPage.selectWeddingYear('2020')
    signUpPage.clickNextButton()
    signUpPage.setGuestsInvited()
    signUpPage.clickNextButton()
    signUpPage.selectEmoticon()
    signUpPage.selectOtherStoresLabel()
    signUpPage.clickNextButton()
    signUpPage.selectFreeShippingLabel()
    signUpPage.selectGiftOption()
    signUpPage.selectThingsTogether()
    signUpPage.clickNextButton()
    signUpPage.selectSpendLabel()
    signUpPage.clickLetsDoThisButton()
  })

  Given(/^I am on the home page$/, function() {
    homePage.open()
    homePage.clickSignUpLink()
    homePage.clickRegistry()
  })

  Given(/^I am already logged into the application$/, function() {
    homePage.open()
    homePage.clickLoginLink()
    loginPage.setEmailFieldText('updatetest1@mailinator.com')
    loginPage.setPasswordFieldText('Password1234')
    loginPage.clickLoginButton()
  })

  When(/^I click start registry$/, function() {
    signUpPage.clickLetsDoThisButton()
  })

  When(/^I navigate to my profile$/, function() {
    homePage.clickYourAccountLink()
  })

})
