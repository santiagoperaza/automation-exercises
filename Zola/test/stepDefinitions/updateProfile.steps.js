import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import loginPage from '../pageobjects/login.page';
import updateProfilePage from '../pageobjects/profile.page';

defineSupportCode(function({ When, Then }) {

  When(/^I update the information with "([^"]*)", "([^"]*)" and "([^"]*)"$/, function(firstName, lastName, email) {
  	updateProfilePage.setFirstNameField(firstName)
  	updateProfilePage.setLastNameField(lastName)
  	updateProfilePage.setEmailField(email)
  	updateProfilePage.clickSaveChangesButton()
  })

  Then(/^I see the success message "([^"]*)"$/, function(assertText) {
  	expect(updateProfilePage.getSuccessMessage()).to.have.string(assertText)
  })

  Then(/^I see the error message "([^"]*)"$/, function(assertText) {
  	expect(updateProfilePage.getErrorMessage()).to.have.string(assertText)
  })

  Then(/^I see the message "([^"]*)" under first name field$/, function(assertText) {
  	expect(updateProfilePage.getFirstNameErrorMessage()).to.have.string(assertText)
  })

  Then(/^I see the message "([^"]*)" under last name field$/, function(assertText) {
  	expect(updateProfilePage.getLastNameErrorMessage()).to.have.string(assertText)
  })

  Then(/^I see the message "([^"]*)" under email field$/, function(assertText) {
  	expect(updateProfilePage.getEmailErrorMessage()).to.have.string(assertText)
  })

})