Feature: Update profile

  As a user I should be able to update my profile.

  Background:
    Given I am already logged into the application

  Scenario Outline: Update profile information with valid data
    When I navigate to my profile
    And I update the information with "<firstName>", "<lastName>" and "<email>" 
    Then I see the success message "Your information has been updated"

    Examples:
      | firstName 		 | lastName 	   | email 						|
      | UpdatedFirstTest | UpdatedLastName | updatetest1@mailinator.com |

  Scenario: Attempt to update profile information with an email already registered
    When I navigate to my profile
    And I update the information with "Test", "Test" and "invalidEmail@mailinator.com" 
    Then I see the error message "There was a problem processing your request"

  Scenario Outline: Attempt to update profile information with an invalid email
    When I navigate to my profile
    And I update the information with "Test", "Test" and "<email>" 
    Then I see the message "Please enter a valid email address." under email field

    Examples:
      | email           |
      | invalidEmailmailinator.com |
      | invalidEmail@mailinator |

  Scenario: Attempt to update profile information without filling first name
	When I navigate to my profile
    And I update the information with "", "Test" and "validEmail@mailinator.com" 
    Then I see the message "This field is required." under first name field

  Scenario: Attempt to update profile information without filling last name
	When I navigate to my profile
    And I update the information with "Test", "" and "validEmail@mailinator.com" 
    Then I see the message "This field is required." under last name field

  Scenario: Attempt to update profile information without filling email
	When I navigate to my profile
    And I update the information with "Test", "Test" and "" 
    Then I see the message "Please enter a valid email address." under email field