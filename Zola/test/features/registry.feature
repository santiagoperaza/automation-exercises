Feature: Registry

  As a new user, I should be able to start the free registry.

  Background:
    Given I am on the home page

  Scenario: Complete the registry
	  When I click start registry
    And I complete the required fields with valid data
    Then I am taken to the next page with text "Get Started With Our Checklist"

  Scenario Outline: Enter a valid wedding date within the next 10 years
    Given I am on the page to enter the wedding date
    When I select the date "<day>"/"<month>"/"<year>"
    Then I do not see "Oops! Please enter a future date." message under wedding date field

    Examples:
      | day | month | year |
      | 01  | June  | 2020 |
      | 01  | June  | 2028 |
      | 01  | June  | 2029 |

  Scenario Outline: Enter a wedding date before actual date
    Given I am on the page to enter the wedding date
    When I select the date "<day>"/"<month>"/"<year>"
    Then I see "Oops! Please enter a future date." message under wedding date field

    Examples:
      | day | month | year |
      | 01  | January  | 2019 |