Feature: Sign up

  As a new user, I should be able to sign up.

  Background:
    Given I am on the page to enter email and password

  Scenario: Sign up with valid credentials
    When I enter valid email and password
      | email           | password     |
      | testing31@mailinator.com | Password1234 |
    Then I am taken to the next page with text "You can't go wrong with these best sellers"

  Scenario Outline: Attempt to sign up with invalid email
    When I enter email "<email>"
    Then I see "Invalid email address" message under email field

    Examples:
      | email           | password     |
      | invalidEmailmailinator.com | Password1234 |
      | invalidEmail@mailinator | Password1234 |

  Scenario: Complete the registry
    When I click start registry
    And I complete the required fields with valid data
    Then I am taken to the next page with text "Get Started With Our Checklist"

  Scenario: Attempt to sign up with a password with less than 8 characters
    When I enter password "1234567"
    Then I see "Password must be at least 8 characters long." message under password field

  Scenario: Attempt to sign up without filling email and password
    When I click sign up
    Then I see "Required" message under email field
    And I see "Required" message under password field

  Scenario: Attempt to sign up without filling email
    When I enter password "12345678"
    And I click sign up
    Then I see "Required" message under email field
    But I do not see "Required" message under password field

  Scenario: Attempt to sign up without filling password
	  When I enter email "testing@mailinator.com"
    And I click sign up
    Then I see "Required" message under password field
    But I do not see "Required" message under email field