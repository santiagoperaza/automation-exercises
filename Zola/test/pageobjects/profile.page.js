import Page from './page'

class ProfilePage extends Page {

  get updateFirstNameField ()	{ return browser.element("input[name='firstName']") }
  get updateLastNameField ()	{ return browser.element("input[name='lastName']") }
  get updateEmailField ()		{ return browser.element("input[name='email']") }
  get saveChangesButton ()		{ return browser.element(".btn.btn-primary.btn-md") }
  get successMessage () 		{ return browser.element("div[style*='display: block;'].humane.humane-zola-success") }
  get errorMessage () 			{ return browser.element("div[style*='display: block;'].humane.humane-zola-error") }
  get firstNameErrorMessage ()	{ return browser.element("input[name='firstName'] ~ .help-block.error-block") }
  get lastNameErrorMessage ()	{ return browser.element("input[name='lastName'] ~ .help-block.error-block") }
  get emailErrorMessage ()		{ return browser.element("input[name='email'] ~ .help-block.error-block") }

  setFirstNameField (firstName) {
  	this.updateFirstNameField.waitForVisible()
  	this.updateFirstNameField.setValue(firstName)
  }

  setLastNameField (lastName) {
  	this.updateLastNameField.waitForVisible()
  	this.updateLastNameField.setValue(lastName)
  }

  setEmailField (email) {
  	this.updateEmailField.waitForVisible()
  	this.updateEmailField.setValue(email)
  }

  clickSaveChangesButton () {
  	this.saveChangesButton.waitForVisible()
  	this.saveChangesButton.click()
  }

  getSuccessMessage () {
  	this.successMessage.waitForExist()
  	if (this.successMessage.isExisting()) {
  		return this.successMessage.getText()
  	} else {
  		return ''
  	}
  }

  getErrorMessage () {
  	this.errorMessage.waitForExist()
  	if (this.errorMessage.isExisting()) {
  		return this.errorMessage.getText()
  	} else {
  		return ''
  	}
  }

  getFirstNameErrorMessage () {
  	this.firstNameErrorMessage.waitForExist()
  	if (this.firstNameErrorMessage.isExisting()) {
  		return this.firstNameErrorMessage.getText()
  	} else {
  		return ''
  	}
  }

  getLastNameErrorMessage () {
  	this.lastNameErrorMessage.waitForExist()
  	if (this.lastNameErrorMessage.isExisting()) {
  		return this.lastNameErrorMessage.getText()
  	} else {
  		return ''
  	}
  }

  getEmailErrorMessage () {
  	this.emailErrorMessage.waitForExist()
  	if (this.emailErrorMessage.isExisting()) {
  		return this.emailErrorMessage.getText()
  	} else {
  		return ''
  	}
  }
}

export default new ProfilePage()