import Page from './page'

class SignUpPage extends Page {

  get letsDoThisButton ()             { return browser.element('.zola-ui.button.primary.lg') }
  get jumpToNextQuestionButton ()     { return browser.element('.onboard-footer__skip') }
  get skipGiftButton ()               { return browser.element('.add-gifts-landing__cta-text') }
  get emailField ()                   { return browser.element('#signup-email') }
  get passwordField ()                { return browser.element('#signup-password') }
  get firstNameField ()               { return browser.element('#user-first-name') }
  get lastNameField ()                { return browser.element('#user-last-name') }
  get spouseFirstNameField ()         { return browser.element('#spouse-first-name') }
  get spouseLastNameField ()          { return browser.element('#spouse-last-name') }
  get weddingDateDayField ()          { return browser.element('#wedding-date-day') }
  get guestsInvitedField ()           { return browser.element('#guests-invited') }
  get nextButton ()                   { return browser.element('.zola-ui.button.lg.primary') }
  get monthDropdown()                 { return browser.element('#wedding-date-month .btn.btn-dropdown.btn-boxed') }
  get yearDropdown()                  { return browser.element('#wedding-date-year .btn.btn-dropdown.btn-boxed') }
  get monthList ()                    { return browser.elements('#wedding-date-month .scroll-select-option') }
  get yearList ()                     { return browser.elements('#wedding-date-year .scroll-select-option') }
  get emoticonLabel ()                { return browser.element('label[for="emotion-eh"]') }
  get otherStoresLabel ()             { return browser.element('label[for="other-stores-no"]') }
  get freeShippingLabel ()            { return browser.element('label[for="ease-of-use"]') }
  get giftsLabel ()                   { return browser.element('label[for="registry-type-gifts"]') }
  get spendLabel ()                   { return browser.element('label[for="around-50"]') }
  get emailValidationText ()          { return browser.element('#signup-email ~ .help-block') }
  get passwordValidationText ()       { return browser.element('#signup-password ~ .help-block') }
  get nextPageText ()                 { return browser.element('div') }
  get weddingDateValidationText ()    { return browser.element('#wedding-date-day ~ .help-block') }
  get cookingOption ()                { return browser.element('label[for="cooking-type"]') }

  printValues (elementParam) {
    console.log("\nEXISTING: " + elementParam.isExisting())
    console.log("VISIBLE: " + elementParam.isVisible())
    console.log("VISIBLE WITHIN VIEWPORT: " + elementParam.isVisibleWithinViewport())
    console.log("ENABLED: " + elementParam.isEnabled())
  }

  clickLetsDoThisButton () {
    this.letsDoThisButton.waitForVisible()
    this.letsDoThisButton.waitForEnabled(2000)
    this.printValues(this.letsDoThisButton)
    this.letsDoThisButton.click()
  }

  clickSkipButton () {
    this.jumpToNextQuestionButton.waitForVisible()
    this.jumpToNextQuestionButton.waitForEnabled(2000)
    this.printValues(this.jumpToNextQuestionButton)
    this.jumpToNextQuestionButton.click()
  }

  clickSkipGiftButton () {
    this.skipGiftButton.waitForVisible()
    this.skipGiftButton.waitForEnabled(2000)
    this.printValues(this.skipGiftButton)
    this.skipGiftButton.click()
    browser.pause(5000)
  }

  setFirstNameField (firstName) {
    this.firstNameField.waitForVisible()
    this.firstNameField.setValue(firstName)
  }

  setLastNameField (lastName) {
    this.lastNameField.waitForVisible()
    this.lastNameField.setValue(lastName)
  }

  setSpouseFirstNameField (firstName) {
    this.spouseFirstNameField.waitForVisible()
    this.spouseFirstNameField.setValue(firstName)
  }

  setSpouseLastNameField (lastName) {
    this.spouseLastNameField.waitForVisible()
    this.spouseLastNameField.setValue(lastName)
  }

  selectWeddingDateDay (dayParam) {
    this.weddingDateDayField.waitForVisible()
    this.weddingDateDayField.setValue(dayParam)
  }

  clickNextButton () {
    this.nextButton.waitForVisible()
    this.nextButton.waitForEnabled(2000)
    this.printValues(this.nextButton)
    this.nextButton.click()
  }

  selectWeddingMonth (monthParam) {
    this.monthDropdown.waitForVisible()
    this.monthDropdown.waitForEnabled(5000)
    this.monthDropdown.click()
    this.monthList.waitForVisible()
    this.monthList.waitForEnabled(5000)
    this.monthList.waitForVisible()
    for (let month of this.monthList.value) {
      if (browser.elementIdText(month.ELEMENT).value == monthParam) {
        browser.elementIdClick(month.ELEMENT)
        break
      }
    }
  }

  selectWeddingYear (yearParam) {
    this.yearDropdown.waitForVisible()
    this.yearDropdown.waitForEnabled(5000)
    this.yearDropdown.click()
    this.yearList.waitForVisible()
    this.yearList.waitForEnabled(5000)
    this.yearList.waitForVisible()
    for (let year of this.yearList.value) {
      if (browser.elementIdText(year.ELEMENT).value == yearParam) {
          browser.elementIdClick(year.ELEMENT)
          break
        }
    }
  }

  selectWeddingDate (dayParam, monthParam, yearParam) {
    this.selectWeddingMonth(monthParam)
    this.selectWeddingDateDay(dayParam)
    this.selectWeddingYear(yearParam)
  }

  setGuestsInvited () {
    this.guestsInvitedField.waitForVisible()
    this.guestsInvitedField.setValue(180)
  }

  selectEmoticon () {
    this.emoticonLabel.waitForVisible()
    this.emoticonLabel.waitForEnabled(2000)
    this.printValues(this.emoticonLabel)
    this.emoticonLabel.click()
  }

  selectGiftOption () {
    this.giftsLabel.waitForVisible()
    this.giftsLabel.waitForEnabled(2000)
    this.printValues(this.giftsLabel)
    this.giftsLabel.click()
  } 

  selectSpendLabel () {
    this.spendLabel.waitForVisible()
    this.spendLabel.waitForEnabled(2000)
    this.printValues(this.spendLabel)
    this.spendLabel.click()
  }

  selectOtherStoresLabel () {
    this.otherStoresLabel.waitForVisible()
    this.otherStoresLabel.waitForEnabled(2000)
    this.printValues(this.otherStoresLabel)
    this.otherStoresLabel.click()
  }

  selectFreeShippingLabel () {
    this.freeShippingLabel.waitForVisible()
    this.freeShippingLabel.waitForEnabled(2000)
    this.printValues(this.freeShippingLabel)
    this.freeShippingLabel.click()
  }

  selectThingsTogether () {
    this.cookingOption.waitForVisible()
    this.cookingOption.waitForEnabled(2000)
    this.printValues(this.cookingOption)
    this.cookingOption.click()
  }

  setEmailField (email) {
    this.emailField.waitForVisible()
    this.emailField.setValue(email)
  }

  setPasswordField (password) {
    this.passwordField.waitForVisible()
    this.passwordField.setValue(password)
  }

  getEmailValidationText () {
    if (this.emailValidationText.isExisting()) {
      this.emailValidationText.waitForVisible()
      browser.waitUntil(() => {
        return this.emailValidationText.getText() != "Checking availability..."
      }, 5000, `Expected text did not appear`, 1000)
      return this.emailValidationText.getText()
    } else {
      return ''
    }
  }

  getPasswordValidationText () {
    if (this.passwordValidationText.isExisting()) {
      this.passwordValidationText.waitForVisible()
      return this.passwordValidationText.getText()
    } else {
      return ''
    }
  }

  getWeddingDateValidationText () {
    if (this.weddingDateValidationText.isExisting()) {
      this.weddingDateValidationText.waitForVisible()
      return this.weddingDateValidationText.getText()
    } else {
      return ''
    }
  }

  getNextPageValidationText () {
    this.nextPageText.waitForVisible()
    return this.nextPageText.getText()
  }

}

export default new SignUpPage()