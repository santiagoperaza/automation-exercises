import Page from './page'

class LoginPage extends Page {

  get emailField ()			{ return browser.element('#login-email') }
  get passwordField ()		{ return browser.element('#unified-nav__password') }
  get loginButton ()		{ return browser.element('.zola-ui.button.full-width-btn.primary.lg') }

  setEmailFieldText (email) {
	this.emailField.waitForVisible()
	this.emailField.setValue(email)
  }

  setPasswordFieldText (password) {
	this.passwordField.waitForVisible()
	this.passwordField.setValue(password)
  }

  clickLoginButton () {
	this.loginButton.waitForVisible()
	this.loginButton.click()
  }

}

export default new LoginPage()