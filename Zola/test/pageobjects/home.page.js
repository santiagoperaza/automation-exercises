import Page from './page'

class HomePage extends Page {

  get logo ()             { return browser.element('.top-nav__logo .nav__logo') }
  get signUpLink ()       { return browser.element('.top-nav__btn') }
  get registryLink ()     { return browser.element('.product-container.tag-signup-intent-registry') }
  get loginLink ()        { return browser.element('.top-nav__login') }
  get yourAccountLink ()  { return browser.element('.top-nav__nav-item .account-link') }

  open () {
    super.open('/')
    this.logo.waitForVisible()
  }

  clickSignUpLink() {
    this.signUpLink.waitForVisible()
    this.signUpLink.click()
  }

  clickRegistry() {
    this.registryLink.waitForVisible()
    this.registryLink.click()
  }

  clickLoginLink () {
    this.loginLink.waitForVisible()
    this.loginLink.click()
  }

  clickYourAccountLink () {
    this.yourAccountLink.waitForVisible()
    this.yourAccountLink.waitForEnabled()
    this.yourAccountLink.click()
  }
}

export default new HomePage()
